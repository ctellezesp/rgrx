import { createSelector } from '@ngrx/store';
import * as PostsReducer from './posts.reducer';

export interface State {
    posts: any;
}

export const reducers = {
    'posts': PostsReducer
}

export const selectPosts = (state: any) => state.posts;

export const selectPostsState = createSelector(
    selectPosts,
    (state) => state.posts
);