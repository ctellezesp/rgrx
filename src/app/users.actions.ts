import { createAction, props } from '@ngrx/store';

export const set = createAction('[User Component] Set', props<{users: any}>());