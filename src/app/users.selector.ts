import { createSelector } from '@ngrx/store';
import * as UserReducer from './users.reducer';

export interface State {
    users: any;
}

export const reducers = {
    'users': UserReducer
}

export const selectUsers = (state: any) => state.users;

export const selectUsersState = createSelector(
    selectUsers,
    (state) => state.users
);