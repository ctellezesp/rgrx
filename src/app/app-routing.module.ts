import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LoginComponent} from './login/login.component';
import {CounterComponent} from './counter/counter.component';
import {UsersComponent} from './users/users.component';

import {UsersResolver} from './users.resolver';
import {PostsResolver} from './posts.resolver';

import {PostsComponent} from './posts/posts.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'counter', component: CounterComponent},
  {path: 'users', component: UsersComponent, resolve: {users: UsersResolver}},
  {path: 'posts', component: PostsComponent, resolve: {posts: PostsResolver}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
