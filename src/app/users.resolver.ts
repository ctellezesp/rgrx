import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot,
         ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import {UsersService} from './users.service';
import { Store, select } from '@ngrx/store';
import {set} from './users.actions';


@Injectable()
export class UsersResolver implements Resolve<any> {
    users$: Observable<number>;
    data$;
  constructor(private usersService: UsersService, private router: Router, private store: Store<{ users: any}>) {
   }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>{
    this.users$ = this.store.pipe(select('users'));
    this.users$.subscribe((res) => {
      this.data$ = res;
    });
    return new Observable(obs => {
      console.log(this.data$);
      debugger;
      if (this.data$.users.length === 0) {
        this.store.pipe(select('users')).subscribe(res => {
          console.log('state', res);
        });
        this.usersService.getUsers().subscribe((res) => {
          this.store.dispatch(set({users: res}));
          console.log('res:', res);
          obs.next(res);
          obs.complete();
        });
      } else {
        this.store.pipe(select('users')).subscribe(res => {
          console.log('state', res);
        });
        obs.next(this.data$.users);
        obs.complete();
      }
    });
  }
}
