import { createReducer, on } from '@ngrx/store';
import * as loginActions from './login.actions';

export const initialState = {
    user: '',
    password: ''
};

const _loginReducer = createReducer(initialState,
    on(loginActions.login, (state,  {username, password} ) => ({ user: username, password: password }))
);

export function logReducer(state, action) {
    return _loginReducer(state, action);
  }