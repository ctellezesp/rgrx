import { createReducer, on } from '@ngrx/store';
import {set} from './users.actions';

export const initialState = {
    users: []
};

const userReducer = createReducer(initialState,
    on(set, (state,  {users}) => ({ users: users}))
);

export function usersReducer(state, action) {
    return userReducer(state, action);
}