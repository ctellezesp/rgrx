import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { usersReducer } from '../users.reducer';
import { postsReducer } from '../posts.reducer';
import { environment } from 'src/environments/environment';

export interface State {
    users: any;
    posts: any;
}
export const reducers: ActionReducerMap<State> = {
    users: usersReducer,
    posts: postsReducer
};
export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
