import { createAction, props } from '@ngrx/store';

export const set = createAction('[Post Component] Set', props<{posts: any}>());