import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { login } from '../login.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  });

  user$: Observable<string>;
  password$: Observable<string>;

  constructor(private store: Store<{ user: string, password: string }>) {
    this.user$ = store.pipe(select('user'));
    this.password$ = store.pipe(select('password'));
   }

  ngOnInit(): void {
  }

  save(){
    this.store.dispatch(login(this.loginForm.value));
  }

}
