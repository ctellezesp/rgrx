import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot,
         ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import {PostsService} from './posts.service';
import { Store, select } from '@ngrx/store';
import {set} from './posts.actions';


@Injectable()
export class PostsResolver implements Resolve<any> {
    posts$: Observable<number>;
    data$;
  constructor(private postsService: PostsService, private router: Router, private store: Store<{ posts: any}>) {
  }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>{
    this.posts$ = this.store.pipe(select('posts'));
    this.posts$.subscribe((res) => {
      this.data$ = res;
    });
    return new Observable(obs => {
      console.log(this.data$);
      debugger;
      if (this.data$.posts.length === 0) {
        this.store.pipe(select('posts')).subscribe(res => {
          console.log('state', res);
        });
        this.postsService.getPosts().subscribe((res) => {
          this.store.dispatch(set({posts: res}));
          console.log('res:', res);
          obs.next(res);
          obs.complete();
        });
      } else {
        this.store.pipe(select('posts')).subscribe(res => {
          console.log('state', res);
        });
        obs.next(this.data$.posts);
        obs.complete();
      }
    });
  }
}
