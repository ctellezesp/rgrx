import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CounterComponent } from './counter/counter.component';

import { ReactiveFormsModule } from '@angular/forms';

import { StoreModule } from '@ngrx/store';
import {logReducer} from './login.reducer';

import { counterReducer } from './counter.reducer';
import { LoginComponent } from './login/login.component';

import {postsReducer} from './posts.reducer';

import {usersReducer} from './users.reducer';

import { HttpClientModule } from '@angular/common/http';
import { UsersComponent } from './users/users.component';

import {UsersResolver} from './users.resolver';
import {PostsResolver} from './posts.resolver';

import {PostsComponent} from './posts/posts.component';
import { reducers, metaReducers } from './reducers';


@NgModule({
  declarations: [
    AppComponent,
    CounterComponent,
    LoginComponent,
    UsersComponent,
    PostsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forRoot({ count: counterReducer }),
    StoreModule.forRoot({ user:  logReducer, password: logReducer}),
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    AppRoutingModule
  ],
  providers: [UsersResolver, PostsResolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
