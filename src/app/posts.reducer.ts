import { createReducer, on } from '@ngrx/store';
import {set} from './posts.actions';

export const initialState = {
    posts: []
};

const postReducer = createReducer(initialState,
    on(set, (state,  {posts}) => ({ posts: posts}))
);

export function postsReducer(state, action) {
    return postReducer(state, action);
}