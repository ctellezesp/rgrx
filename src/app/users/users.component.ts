import { Component, OnInit } from '@angular/core';
import {UsersService} from '../users.service';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import {selectUsersState} from '../users.selector';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(private usersService: UsersService, private store: Store<{ users }>, private route: ActivatedRoute) { }

  users: any;

  ngOnInit(): void {
    this.users = this.route.snapshot.data.users;
    console.log(this.users);
  }

}
